# eslint-config-autoavaliar

Shareable ESLint config

## Install

```sh
npm i --save-dev eslint-config-autoavaliar
```

## Usage

Shareable configs are designed to work with the `extends` feature of `.eslintrc` files.
You can learn more about [Shareable Configs](http://eslint.org/docs/developer-guide/shareable-configs) on the official ESLint website.

Add this to your `.eslintrc` file (or `package.json`):

```json
{
  "extends": "autoavaliar"
}
```

_Note: We omitted the `eslint-config-` prefix since it is automatically assumed by ESLint._

You can override settings from the shareable config by adding them directly into your
`.eslintrc` file.
